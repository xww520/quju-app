import Vue from 'vue'
import Vuex from 'vuex'
import fetch from '../common/request.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		version: "1.0.0",
		// 经度,默认深圳
		longitude: uni.getStorageSync("longitude") || '114.010857',
		// 纬度,默认深圳
		latitude: uni.getStorageSync("latitude") || '22.63137',
		// 定位城市
		cityName: uni.getStorageSync("cityName") || '定位中...',
		//行政区编码
		cityCode: uni.getStorageSync("cityCode") || '',

		token: uni.getStorageSync("token") || 0,
		isLogin: uni.getStorageSync("token") ? true : false,
		// 是否商家
		isMer: uni.getStorageSync("isMer") || 0,
		adCoopenListVisible: uni.getStorageSync("adCoopenListVisible") ? true : false,

		memberId: uni.getStorageSync("memberId") || '',
		eduType: uni.getStorageSync("eduType") || '',
		userName: uni.getStorageSync("userName") || '未获取到',
		headPic: uni.getStorageSync("headPic") || '',
		photos: uni.getStorageSync("photos") || '',
		sign: uni.getStorageSync("sign") || '',
		hobTag: uni.getStorageSync("hobTag") || '',
		sex: uni.getStorageSync("sex") || 0,
		affectiveState: uni.getStorageSync("affectiveState") || 0,
		birthday: uni.getStorageSync("birthday"),
		provinceName: uni.getStorageSync("provinceName"),
		provinceCode: uni.getStorageSync("provinceCode"),

		provinceNameReal: uni.getStorageSync("provinceNameReal"),
		provinceCodeReal: uni.getStorageSync("provinceCodeReal"),
		cityNameReal: uni.getStorageSync("cityNameReal"),
		cityCodeReal: uni.getStorageSync("cityCodeReal"),
		areaNameReal: uni.getStorageSync("areaNameReal"),
		areaCodeReal: uni.getStorageSync("areaCodeReal"),

		wxNo: uni.getStorageSync("wxNo"),
		postJob: uni.getStorageSync("postJob"),
		height: uni.getStorageSync("height"),
		weight: uni.getStorageSync("weight"),
		headAudit: uni.getStorageSync("headAudit") == 1 ? true : false,
		registerChannel: uni.getStorageSync("registerChannel"),
		registerNo: uni.getStorageSync("registerNo"),
		actTypeList: uni.getStorageSync("actTypeList"),

		// 当前大概位置名称
		curLocalAddress: uni.getStorageSync("curLocalAddress") || uni.getStorageSync("cityName"),
		historyStr: uni.getStorageSync("historyStr") || '',
		isPublishPlaza: false,

		/* 		userInfo: { // 用户信息
					longitude: null, // 经度
					latitude: null, // 纬度
					locationCity: '', // 定位城市
					sex:uni.getStorageSync("userInfo_sex"),
					adcode:''  //行政区编码,
				}, */
		appInfo: { // APP整体数据
			commonDataVersion: '0', // 公共数据的大版本号
			adVersion: '0', // 广告数据版本号	
			serviceVersion: '0' // 服务数据版本号
		},
		adData: {
			homePageAdverts: [],
			carPageAdverts: [],
			servicePageAdverts: []
		},
		modulesData: { // 首页服务模块、产品模块和服务页服务模块数据
			serviceModules: [],
			productModules: [],
			serviceFuncVOList: []
		}
	},
	mutations: {
		login(state, token) {
			if (token) {
				uni.setStorageSync('token', token);
				state.token = token;
			}
			state.isLogin = true;
		},
		logout(state) {

			state.token = "";
			state.isLogin = false;
			state.isMer = 0;
			state.adCoopenListVisible = "";

			// 其他跟用户有关字段都清楚
			state.memberId = '',
				state.memberIdForMe = '',
				state.userName = '',
				state.cityCode = '',
				state.cityName = '',
				state.headPic = '',
				state.photos = '',
				state.sign = '',
				state.hobTag = '',
				state.sex = '',
				state.affectiveState = '',
				state.eduType = '',
				state.birthday = '',
				state.provinceName = '',
				state.provinceCode = '',
				state.districtName = '',
				state.districtCode = '',
				state.wxNo = '',
				state.postJob = '',
				state.weight = '',
				state.height = '',
				state.headAudit = '',
				state.registerChannel = '',
				state.registerNo = '',
				state.actTypeList = '',

				state.provinceNameReal = '',
				state.provinceCodeReal = '',
				state.districtNameReal = '',
				state.districtCodeReal = '',
				state.areaNameReal = '',
				state.areaCodeReal = '',

				uni.clearStorageSync();
		},
		setCurLocalAddress(state, curLocalAddress) {
			uni.setStorageSync('curLocalAddress', curLocalAddress);
			state.curLocalAddress = curLocalAddress;
		},
		setAdCoopenListVisible(state, adCoopenListVisible) {
			uni.setStorageSync('adCoopenListVisible', adCoopenListVisible);
			state.adCoopenListVisible = adCoopenListVisible;
		},
		setHistoryStr(state, historyStr) {
			uni.setStorageSync('historyStr', historyStr);
			state.historyStr = historyStr;
		},
		setIsPublishPlaza(state, booleanValue) {
			state.isPublishPlaza = booleanValue;
		},
		setUserInfo(state, payload) {
			if (payload) {
				for (let i in payload) {
					if ((payload[i] != undefined && payload[i] != null && payload[i] != '') || typeof payload[i] === 'number') {
						uni.setStorageSync(i, payload[i]);
						state[i] = payload[i];
					}
				}
			}
		},
		// 设置用户信息
		/* 		setUserInfo(state, payload) {
					for (let i in payload) {
						for (let j in state.userInfo) {
							if (i == j) {
								uni.setStorageSync('userInfo_'+j, payload[i]);
								state.userInfo[j] = payload[i].data;
							}
						}
					}
				}, */
		// 设置APP信息
		setAppInfo(state, payload) {
			for (let i in payload) {
				for (let j in state.appInfo) {
					if (i === j) {
						state.appInfo[j] = payload[i]
					}
				}
			}
		},
		// 更新APP整体广告数据
		updateAdData(state, payload) {
			uni.setStorageSync("carPageAdverts", JSON.stringify(payload.adData.carPageAdverts));
			state.adData = payload.adData
		},
		// 更新APP整体服务数据
		updateModulesData(state, payload) {
			state.modulesData = payload.modulesData
		}
	},
	actions: {
		// 查看公共数据版本是否更新
		// 返回有data数据时代表有更新，未返回data数据代表不需要更新
		// 返回9010错误时代表还未生成公共数据版本，需要先调用对应接口生成数据版本号
		async checkModuleUpdate({
			commit,
			state
		}) {
			return new Promise((resolve, reject) => {
				fetch.request('config/queryHasUpdates', {
					version: state.appInfo.commonDataVersion
				}, 'POST').then((res) => {
					//console.log('各个模块数据数据' + JSON.stringify(res))
					if (res.code === 200) {
						if (!res.data) return
						const data = res.data.versionData
						let obj = {
							updateAd: false,
							updateService: false
						}
						commit('setAppInfo', {
							commonDataVersion: res.data.version
						})
						if (data.advertVersion !== state.appInfo.adVersion) {
							obj.updateAd = true
						}
						if (data.serviceVersion !== state.appInfo.serviceVersion) {
							obj.updateService = true
						}
						resolve(obj)
					} else {
						fetch.toast(res.message)
					}
				}).catch((e) => {})
			})
		},
		// 查询广告（首页、车圈、服务）
		getAds({
			dispatch,
			commit,
			state
		}) {
			fetch.request('config/queryAdverts', {
				clientDictKey: 1,
				version: state.appInfo.adVersion
			}, 'POST').then((res) => {
				if (res.code === 200) {
					commit('updateAdData', {
						adData: res.data
					})
					commit('setAppInfo', {
						adVersion: res.data.version
					})
				} else {
					fetch.toast(res.message)
				}
			}).catch((e) => {})
		},
		// 获取首页服务模块、产品模块和服务页服务模块数据
		getServices({
			commit,
			state
		}) {
			fetch.request('serviceFunc/searchServiceFunc', {
				version: state.appInfo.serviceVersion
			}, 'POST').then((res) => {
				// console.log('首页服务模块、产品模块和服务页服务模块数据' + JSON.stringify(res.data))
				if (res.code === 200) {
					commit('updateModulesData', {
						modulesData: res.data.data
					})
					commit('setAppInfo', {
						serviceVersion: res.data.version
					})
				} else {
					fetch.toast(res.message)
				}
			}).catch((e) => {})
		},
	}
})

export default store
