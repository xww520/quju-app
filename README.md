# quju-app 【小猪趣聚】-小程序

#### 介绍

【小猪趣聚】是一个社交平台，让更能多的人享受聚会的乐趣，一个有趣的同城聚会平台！

为什么还开发【小猪趣聚】社交APP？（项目已停服，现全部贡献源码）

据数据调查显示，目前城市单身青年想要脱单现状：
下载注册各种相亲或社交APP，纯线上模式，变相充值或购买服务，但还会遇盗各种虚假会员和托；（网聊不靠谱，充值服务费用高）
参加相亲机构组织策划的脱单活动；（费用高，相亲太直接，部分人员信息不对等，如年龄，职业，收入，学历等）
进自媒体或微博号提供的各种收费单身微信群；（群里成员参差不齐，还收费；没人组织线下活动）
加入各种兴趣爱好群；（加的群多，潜水的多，出去线下见面认识的少）
同学圈子，同事圈子，家人圈子，因这些圈子都有限，脱单效率低下；

【小猪趣聚】正逐一解决以上问题而开发的一款新社交软件，通过
线上大数据AI匹配对象，线下撮合聚会，让大数据在相亲社交领域赋能，减少无效无意义社交！

使命：
互帮互助，众人拾柴火焰高，帮助城市单身青年，扩大社交圈子，认识更多异性和朋友，告别枯躁乏味的宅家时间，加入更多线下吃喝玩乐兴趣活动，让每个人的生活多一份乐趣！趣生活，趣同城！
愿景：
做中国最大的纯净真实，公益免费，非盈利性交友平台；让相亲找对象这事变得更简单，我为人人，人人为我，互帮互助，公益免费；
价值观：
真诚友善，我为人人，人人为我，互帮互助；

#### 功能模块

支持在线发布活动，参加兴趣社群，发布活动感悟回放，搜索附件的人，基于webSocket开发的聊天系统，商户入驻，商家发放优惠券，用户可领券去线下消费，发布旅游笔记

分享活动赚取佣金，广场动态，会员积分登记制度，裂变分享；粉丝，关注，收藏等功能。理想很丰满，现实很骨感，写这个项目当做练手学习，最后商业逻辑没跑通；定位不清晰等原因；
停止运营了（也可以说一开始就没正式运营，只是小圈子内推广试用）

### 技术架构

* 前端使用VUE框架，基于uniapp框架开发的，一次开发，多端可用，支持微信小程序，头条抖音小程序，h5端，及打包成android app,ios;（最终是在微信小程序平台运行）
* 工程里面拥有各种功能组件，AI通过照片测试你的颜值（使用旷视AI人脸识别接口实现），小程序分享接口，生成自定义分享海报，接入微信登录快键登录；日历组件，级联选择组件等；
* 基于webSocket实现的在线聊天功能；
* 后端采用springboot框架，前后端接口权限校验，使用JWT; 集成redis，热点数据放redis;后端接口支持自动生成；便于快速开发业务功能，并快速上线；

* 如需前端项目请查看链接 ，请移步 [quju-app](https://gitee.com/xww520/quju-app)
* 如需小猪趣聚的后台运营管理系统  [AppAdmin](https://gitee.com/xww520/RuoYi-AppAdmin)
* 如需API后端服务接口项目请查看链接 ，请移步 [AppApi](https://gitee.com/xww520/RuoYi-AppApi)
* 【毫无保留给个人及企业免费使用；拥抱开源，让技术实现更多的价值】

#### 安装教程

1. npm install
2. npm run dev 即可运行
3. npm run build 打包，如需打包android或ios版，和发布小程序到微信平台，请使用工具HBuilderX，下载微信官网的调试工具；

#### 系统截图

<table>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/1.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/2.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/3.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/4.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/5.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/6.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/7.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/8.jpg"/></td>
    </tr>	 
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/9.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/10.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/11.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/12.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/13.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/14.jpg"/></td>
    </tr>
</table>

#### 使用说明

1. 安装nodejs

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支【dev-v5】
3. 提交代码
4. 新建 Pull Request

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
